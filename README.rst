=====
Django Auth Google
=====

Auth Google is a Django app to Sign up / Log in using Google SignIn

Quick start
-----------

1. Add "google_auth" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'google_auth.apps.GoogleAuthConfig',
    ]

2. Set the facebook app id and secret variables::

    GOOGLE_CLIENT_IDS = variables['GOOGLE_CLIENT_IDS']

3. Create a subclass of ``GoogleAuth`` overriding the method ``get_user``::

    class ProjectGoogleAuth(GoogleAuth):

        def get_user(self, info):
            User.objects.create_user(username, info['email'], first_name=info["given_name"],
                                     last_name=info["family_name"])

In this method you get or create an instance of ``django.contrib.auth.models.User`` and make any additional configuration
needed by your project.

4. Run ``python manage.py migrate`` to create the google-auth models.

