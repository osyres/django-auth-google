from django.contrib import admin
from django.contrib.admin import register

from google_auth.models import GoogleUser


@register(GoogleUser)
class GoogleUserAdmin(admin.ModelAdmin):
    list_display = ['google_id', 'user']
