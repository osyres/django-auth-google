from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class GoogleAuthConfig(AppConfig):
    name = 'google_auth'

    verbose_name = _('Google Auth')
