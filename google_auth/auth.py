from abc import ABC, abstractmethod

from django.conf import settings
from django.utils.translation import gettext_lazy as _
from google.auth.transport import requests
from google.oauth2 import id_token

from core.exceptions import UserInputError
from google_auth.models import GoogleUser


class GoogleAuth(ABC):

    def login_or_signup(self, token):
        info = self.validate_token(token)
        try:
            google_user = GoogleUser.objects.get(google_id=info['sub'])
            return self._update_token(token, google_user), False
        except GoogleUser.DoesNotExist:
            return self.signup(token, info), True

    def _update_token(self, token, google_user):
        google_user.token = token
        google_user.save(update_fields=['token'])
        return google_user

    def signup(self, token, info=None):
        info = info or self.validate_token(token)
        user = self.get_user(info)
        return GoogleUser.objects.create(user=user, google_id=info['sub'], token=token)

    @abstractmethod
    def get_user(self, info):
        pass

    def validate_token(self, token):
        try:
            info = id_token.verify_oauth2_token(token, requests.Request())
            if info['aud'] not in settings.GOOGLE_CLIENT_IDS:
                raise UserInputError(_("It wasn't possible to validate the account"), 'google.invalid_client')
            return info
        except ValueError as error:
            raise UserInputError(str(error), 'google.token')
