from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


class GoogleUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=_('User'))
    google_id = models.CharField(max_length=32, unique=True, verbose_name=_('Google Id'))
    token = models.TextField(_('Token'))
    created = models.DateTimeField(_('Created'), auto_now_add=True)
