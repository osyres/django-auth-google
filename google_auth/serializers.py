from rest_framework import serializers


class GoogleLoginDeserializer(serializers.Serializer):
    token = serializers.CharField()

    def create(self, validated_data):
        return validated_data['token']
